#!/usr/bin/env bash
# This script export one tabular file with the disk usage and quota : disk_usage_quota.tab
# account  usage(GB)  soft_quota(GB)  status_bar
# It can be cronable and the file can be store and accessible to the status_bars main script


SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

if [ -z "$TAB_PATH" ]; then
  TAB_PATH="."
fi

DATE=$(date +"%Y-%m-%d")

> $TAB_PATH/disk_usage_quota_${DATE}.tab
for PROJECT in $PROJECT_HOME/*; do
    PROJECT_BASENAME=$(basename $PROJECT)

    SIZE=$(mfsgetquota -m $PROJECT | grep ' size ')

    USAGE=$(toGB $(echo $SIZE | cut -d "|" -f 2 | sed "s/ //g") MB)
    SOFTQUOTA=$(toGB $(echo $SIZE | cut -d "|" -f 3| sed "s/ //g") MB)

    STATUS_BAR=$(status_bar $USAGE $SOFTQUOTA)

    echo -e $PROJECT_BASENAME"\t"$USAGE"\t"$SOFTQUOTA"\t"$STATUS_BAR >> $TAB_PATH/disk_usage_quota_${DATE}.tab
done
ln -sf $TAB_PATH/disk_usage_quota_${DATE}.tab $TAB_PATH/disk_usage_quota.tab
