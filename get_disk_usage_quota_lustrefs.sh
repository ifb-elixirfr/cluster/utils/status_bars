#!/usr/bin/env bash
# This script export one tabular file with the disk usage and quota : disk_usage_quota.tab
# account  usage(GB)  soft_quota(GB)  status_bar
# It can be cronable and the file can be store and accessible to the status_bars main script


SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

DATE=$(date +"%Y-%m-%d")

function get_quota() {
    DIRECTORY=$1
    DIRECTORY_BASENAME=$(basename $DIRECTORY)
    GID=$(getent group $DIRECTORY_BASENAME | cut -f 3 -d ':')

    # sed "s/*//" -> remove * when the quota soft is exceeded
    SIZE=$(lfs quota -p $GID $DIRECTORY | sed -n "4p" | sed "s/*//")

    USAGE_BYTES=$(toGB $(echo $SIZE | awk '{print $1}') KB)
    SOFTQUOTA_BYTES=$(toGB $(echo $SIZE | awk '{print $2}') KB)
    STATUS_BAR_BYTES=$(status_bar $USAGE_BYTES $SOFTQUOTA_BYTES)

    USAGE_FILES=$(echo $SIZE | awk '{print $5}')
    SOFTQUOTA_FILES=$(echo $SIZE | awk '{print $6}')
    STATUS_BAR_FILES=$(status_bar $USAGE_FILES $SOFTQUOTA_FILES)

    echo -e $DIRECTORY_BASENAME"\t"$USAGE_BYTES"\t"$SOFTQUOTA_BYTES"\t"$STATUS_BAR_BYTES"\t"$USAGE_FILES"\t"$SOFTQUOTA_FILES"\t"$STATUS_BAR_FILES
}

> $TAB_PATH/disk_usage_quota_${DATE}.tab
for PROJECT in $PROJECT_HOME/*; do
    get_quota $PROJECT >> $TAB_PATH/disk_usage_quota_${DATE}.tab
done
ln -sf $TAB_PATH/disk_usage_quota_${DATE}.tab $TAB_PATH/disk_usage_quota.tab


> $TAB_PATH/disk_usage_quota_home_${DATE}.tab
for HOME in $HOME_HOME/*; do
    get_quota $HOME >> $TAB_PATH/disk_usage_quota_home_${DATE}.tab
done
ln -sf $TAB_PATH/disk_usage_quota_home_${DATE}.tab $TAB_PATH/disk_usage_quota_home.tab
