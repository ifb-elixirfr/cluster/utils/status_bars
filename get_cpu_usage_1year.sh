#!/usr/bin/env bash

source $STATUS_BAR_CONF_PATH

if [ -z "$TAB_PATH" ]; then
  TAB_PATH="."
fi

START=$(date +"%Y-%m-%d"  -d "1 year ago")
END=$(date +"%Y-%m-%d")
sreport -n -P -t hour Cluster AccountUitlizationByUser  format=Account,Used start=$START end=$END | awk -F "|" '{ if (!($1 not in accounts)) accounts[$1] = $2;} END { for (account in accounts) print account"\t"accounts[account] }' > $TAB_PATH/cpu_usage_${START}_${END}.tab
