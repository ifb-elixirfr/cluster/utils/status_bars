#!/usr/bin/env bash
# This script export one tabular file with the SLURM CPU usage and quota : cpu_usage_quota.tab
# account  usage(hCPU)  quota(hCPU)  status_bar
# It can be cronable and the file can be store and accessible to the status_bars main script



SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

if [ -z "$TAB_PATH" ]; then
  TAB_PATH="."
fi

DATE=$(date +"%Y-%m-%d")

sshare -n -P --format=Account,RawUsage,GrpTRESMins | grep "^ [a-zA-Z0-9]" | awk -F "|" '$3 ~ /cpu=/ { gsub("cpu=","",$3) } { if ($3 == "") $3 = 60*10000; printf "%s\t%d\t%d\n", $1,$2/60,$3/60 }' | sed "s/^ //" > $TAB_PATH/cpu_usage_quota_${DATE}.tab.tmp

> $TAB_PATH/cpu_usage_quota_${DATE}.tab
while IFS=$'\t' read PROJECT CPU_USAGE CPU_QUOTA; do
  CPU_STATUS_BAR=$(status_bar $CPU_USAGE $CPU_QUOTA)
  echo -e $PROJECT"\t"$CPU_USAGE"\t"$CPU_QUOTA"\t"$CPU_STATUS_BAR >> $TAB_PATH/cpu_usage_quota_${DATE}.tab
done < $TAB_PATH/cpu_usage_quota_${DATE}.tab.tmp


rm $TAB_PATH/cpu_usage_quota_${DATE}.tab.tmp

ln -sf $TAB_PATH/cpu_usage_quota_${DATE}.tab $TAB_PATH/cpu_usage_quota.tab
