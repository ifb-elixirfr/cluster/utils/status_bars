#!/usr/bin/env bash

export BAR_COLOR_THRESHOLD1=9
export BAR_COLOR_THRESHOLD2=14
export INVERSE=TRUE

# Last week from saturday to friday
SINCE=$(date -d 'last week' '+%Y-%m-%dT%H:%M')
UNTIL=$(date -d 'now' '+%Y-%m-%dT%H:%M')
DATE=$(date +"%Y-%m-%d")


SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

if [ -z "$TAB_PATH" ]; then
  TAB_PATH="."
fi

OUTPUT_TAB=$TAB_PATH/reportseff_${SINCE}_${UNTIL}.tab

#module load reportseff/2.7.6
REPORTSEFF_PATH=/shared/software/miniconda/envs/reportseff-2.7.6/bin/reportseff

function get_metric() {
   echo "$REPORTSEFF" | (grep $1 || echo -e "$1\t0") | cut  -f2 -d " "
}

function reportseff_per_user() {
    $REPORTSEFF_PATH -p -u $1 -g --since "${SINCE}" --until "${UNTIL}" --format "+ReqCPUS,ReqMem" |
        sed "1d" | 
        awk -F "|" '
            BEGIN { STATUS["COMPLETED"]=0; STATUS["CANCELED"]=0; STATUS["FAILED"]=0; STATUS["OUT_OF_MEMORY"]=0 }
            { STATUS[$2]++ }
            $2 == "COMPLETED" { TimeEff+=$4; CPUEff+=$5; MemEff+=$6 }
            END { 
                for (S in STATUS) { print S,STATUS[S] }
                if (STATUS["COMPLETED"] == 0) { STATUS["COMPLETED"] = 1 }  
                printf "TimeEff %.0f\n",TimeEff/STATUS["COMPLETED"]
                printf "CPUEff %.0f\n",CPUEff/STATUS["COMPLETED"]
                printf "MemEff %.0f\n",MemEff/STATUS["COMPLETED"]
            }
        '
}

SACCT_USERS=$(sacct -XPn -S "${SINCE}" -E "${UNTIL}" -a --format "User" | sort | uniq)

> $OUTPUT_TAB
while IFS= read -r USERSLURM ; do 
    echo $USERSLURM; 

    REPORTSEFF=$(reportseff_per_user $USERSLURM)

    TimeEff=$(get_metric TimeEff)
    TimeEff_STATUS=$(status_bar $TimeEff 100)

    CPUEff=$(get_metric CPUEff)
    CPUEff_STATUS=$(status_bar $CPUEff 100)

    MemEff=$(get_metric MemEff)
    MemEff_STATUS=$(status_bar $MemEff 100)

    COMPLETED=$(get_metric COMPLETED)
    CANCELED=$(get_metric CANCELED)
    FAILED=$(get_metric FAILED)
    OUT_OF_MEMORY=$(get_metric OUT_OF_MEMORY)

    echo -e $USERSLURM"\t"$TimeEff"\t"$TimeEff_STATUS"\t"$CPUEff"\t"$CPUEff_STATUS"\t"$MemEff"\t"$MemEff_STATUS"\tCO:"$COMPLETED"\tCA:"$CANCELED"\tF:"$FAILED"\tOOM:"$OUT_OF_MEMORY >> $OUTPUT_TAB
done <<< "$SACCT_USERS"

ln -sf $OUTPUT_TAB $TAB_PATH/reportseff.tab
