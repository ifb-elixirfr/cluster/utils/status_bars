#!/usr/bin/env bash
# This script export one tabular file with the disk usage and quota : disk_usage_quota.tab
# account  usage(GB)  soft_quota(GB)  status_bar
# It can be cronable and the file can be store and accessible to the status_bars main script


SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $SCRIPTPATH/lib.sh
source $STATUS_BAR_CONF_PATH

if [ -z "$TAB_PATH" ]; then
  TAB_PATH="."
fi

DATE=$(date +"%Y-%m-%d")

> $TAB_PATH/disk_usage_quota_${DATE}.tab
for PROJECT in $PROJECT_HOME/*; do
    PROJECT_BASENAME=$(basename $PROJECT)
    
    SIZE=$(ssh ${STORAGE_SERVER_USER}@${STORAGE_SERVER_NAME} NAS-volumes volumes $STORAGE_SERVER_VOLUME quota directory view /$PROJECT_BASENAME 2> /dev/null)

    USAGE=$(toGB $(echo "$SIZE" | grep "Usage" | awk '{print $3" "$4}'))
    SOFTQUOTA=$(toGB $(echo "$SIZE" | grep "Soft Limit" | awk '{print $4" "$5}'))
    HARDQUOTA=$(toGB $(echo "$SIZE" | grep "Hard Limit" | awk '{print $4" "$5}'))
    if [ $SOFTQUOTA -eq 0 ]; then
      SOFTQUOTA=$HARDQUOTA
    fi

    STATUS_BAR=$(status_bar $USAGE $SOFTQUOTA)

    echo -e $PROJECT_BASENAME"\t"$USAGE"\t"$SOFTQUOTA"\t"$STATUS_BAR >> $TAB_PATH/disk_usage_quota_${DATE}.tab
done
ln -sf $TAB_PATH/disk_usage_quota_${DATE}.tab $TAB_PATH/disk_usage_quota.tab
